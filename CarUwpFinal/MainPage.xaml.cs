﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;
using CarLibraryFinal;
using Windows.UI.Xaml.Automation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace CarUwpFinal
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private CarRepository _carRepository = new CarRepository();
        public MainPage()
        {
            this.InitializeComponent();
        }

    //Method to receive user's input in the given textBoxes and Combobxes and returns to the instance of Car class.
        private Car CaptureCarInfo()
        {
            string vinNumber = TxtCarVin.Text;
            string carMake = TxtCarMake.Text;
            float purchasePrice = float.Parse(TxtPurchasePrice.Text);
            int modelYear = int.Parse(CmbModelYear.SelectedItem.ToString());
            int mileage = int.Parse(TxtMileage.Text);
            CarType carType = (CarType)CmbCarType.SelectedItem;
            Car c = new Car(vinNumber, carMake, purchasePrice, carType, modelYear, mileage);
            return c;
        }

        //Property to bind the Enum to the ComboBox
        public IList<CarType> CarTypes
        {
            get { return Enum.GetValues(typeof(CarType)).Cast<CarType>().ToList<CarType>(); }
        }
        public CarType CarType
        {
            get;
            set;
        }

        private void RenderCarInfo(Car car)
        {
            TxtCarVin.Text = car.VinNumber;
            TxtCarMake.Text = car.CarMake;
            TxtPurchasePrice.Text = car.PurchasePrice.ToString();
            CmbCarType.SelectedItem = car.CarTypeN.ToString();
            CmbModelYear.SelectedItem = car.ModelYear.ToString();
            TxtMileage.Text = car.Mileage.ToString();

    // Disaplaying the car images when user select's a particular car images are displayed on basis of index
            if (CmbCarType.SelectedIndex==0)
            {
                ImgUsersChoice.Source = new BitmapImage
                    (new Uri($"ms-appx:///Assets/CarImages/VinNumber01.jpg"));
            }
            if (CmbCarType.SelectedIndex == 1)
            {
                ImgUsersChoice.Source = new BitmapImage
                    (new Uri($"ms-appx:///Assets/CarImages/VinNumber02.jpg"));
            }
            if (CmbCarType.SelectedIndex == 2)
            {
                ImgUsersChoice.Source = new BitmapImage
                    (new Uri($"ms-appx:///Assets/CarImages/VinNumber03.jpg"));
            }
            if (CmbCarType.SelectedIndex == 3)
            {
                ImgUsersChoice.Source = new BitmapImage
                    (new Uri($"ms-appx:///Assets/CarImages/VinNumber04.jpg"));
            }
            if (CmbCarType.SelectedIndex == 4)
            {
                ImgUsersChoice.Source = new BitmapImage
                    (new Uri($"ms-appx:///Assets/CarImages/VinNumber05.jpg"));
            }
        }

        //Clearing the textboxes

        private void ClearUI()
        {
            TxtCarVin.Text = "";
            TxtCarMake.Text = "";
            TxtPurchasePrice.Text = "";
            CmbCarType.SelectedItem = "";
            CmbModelYear.SelectedItem = "";
            TxtMileage.Text = "";
            TxtErrorMessage.Text = " ";
        }

        //Clear methok will be invoked on the click of clear Button
        private void OnClear(object sender, RoutedEventArgs e)
        {
            ClearUI();
        }

        //when the add car button is clicked details of cars will be shown in listBox
        private void OnAddCar(object sender, RoutedEventArgs e)
        {
            try
            {
                Car c = CaptureCarInfo();
                _carRepository.AddCar(c);
                LstCars.Items.Add(c);
            }
            catch (Exception error)
            {
                TxtErrorMessage.Text = error.Message;
            }
        }

        //Get the details of selected cars and rendering to the input fields
        private void OnCarSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Car c = (Car)LstCars.SelectedItem;
            RenderCarInfo(c);
        }

        //private void OnUpdateCar(object sender, RoutedEventArgs e)
        //{
        //    if (TxtCarVin.Text==TxtCarVin.Text)
        //    {
        //        LstCars.Items.Add(c);
        //    }
        //}
    }
}
