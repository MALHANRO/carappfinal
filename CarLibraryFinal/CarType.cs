﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarLibraryFinal
{

    //Enum created to give user choice to select from the given carTypes
    public enum CarType
    {
    Sedan,
    Hatchback,
    Coupe,
    Crossover,
    Convertible,
    }
}
